# Teste Lógica Apisul #

---

Suponha que a administração do prédio 99a da Tecnopuc, com 16 andares e cinco elevadores, denominados A, B, C, D e E, nos convidou a aperfeiçoar o sistema de controle dos elevadores. Depois de realizado um levantamento no qual cada usuário respondia:

- O elevador que utiliza com mais frequência (A, B, C, D ou E);
- O andar ao qual se dirigia (0 a 15);
- O período que utilizava o elevador – M: Matutino; V: Vespertino; N: Noturno.

Considerando que este possa evoluir para um sistema dinâmico, escreva o código, em C#, que nos ajude a extrair as informações:

* Qual é o andar menos utilizado pelos usuários;
* Qual é o elevador mais frequentado e o período que se encontra maior fluxo;
* Qual é o elevador menos frequentado e o período que se encontra menor fluxo;
* Qual o período de maior utilização do conjunto de elevadores;
* Qual o percentual de uso de cada elevador com relação a todos os serviços prestados;

### ENTRADAS
11AM  
12AM  
14AM  
0AM  
1AM  
15BM  
13BM  
1CM  
2CM  
4CM  
3CM  
4CM  
5DM  
6EM  
7AM  
10AM  
9AM  
15BV  
13BV  
1CV  
2CV  
4CN  
3CV