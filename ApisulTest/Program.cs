﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ApisulTest
{
    class Program
    {
        private static int QTDE_ANDARES = 15;
        private static String[] ELEVADORES = { "A", "B", "C", "D", "E" };

        static void Main(string[] args)
        {
            List<UserInput> inputs = readInput();
            if (inputs.Count() > 0)
            {
                Console.WriteLine("---- Informações ----");
                printAndarMenosUtilizado(inputs);
                printElevadorMaisFrequentado(inputs);
                printElevadorMenosFrequentado(inputs);
                printPeriodoMaiorUtilizacao(inputs);
                printPercentualUsoElevadores(inputs);
            }
            else
            {
                Console.WriteLine("-- Nenhuma entrada válida informada --");
            }

            Console.WriteLine("Aperte qualquer tecla para encerrar.");
            Console.ReadKey();
        }

        private static void printPercentualUsoElevadores(List<UserInput> inputs)
        {
            Console.WriteLine("Percentual de uso de cada Elevador:");
            int total = inputs.Count();
            foreach (string elevador in ELEVADORES)
            {
                Console.WriteLine("     Elevador {0} - {1:#00.00}%", elevador, inputs.Count(c => c.elevador == elevador) * 100f / total);
            }
        }

        // Busca o Periodo de maior utilização
        private static void printPeriodoMaiorUtilizacao(List<UserInput> inputs)
        {
            PeriodoEnum result = inputs.GroupBy(p => p.periodo).OrderByDescending(c => c.Count()).First().Key;
            Console.WriteLine("Periodo com maior utilização dos elevadores é o {0}.", result);
        }

        //Busca pelos elevadores menos frequentados dentre os informados
        private static void printElevadorMenosFrequentado(List<UserInput> inputs)
        {
            string elevador = inputs.GroupBy(e => e.elevador).OrderBy(c => c.Count()).First().Key;
            PeriodoEnum periodo = inputs.FindAll(e => e.elevador == elevador).GroupBy(p => p.periodo).OrderBy(c => c.Count()).First().Key;
            Console.WriteLine("Elevador menos frequentado é o {0}, o período que se encontra menor fluxo é o {1}.", elevador, periodo);
        }

        // Busca pelo elevador mais frequentado e o periodo de maior fluxo
        private static void printElevadorMaisFrequentado(List<UserInput> inputs)
        {
            string elevador = inputs.GroupBy(e => e.elevador).OrderByDescending(c => c.Count()).First().Key;
            PeriodoEnum periodo = inputs.FindAll(e => e.elevador == elevador).GroupBy(p => p.periodo).OrderByDescending(c => c.Count()).First().Key;
            Console.WriteLine("Elevador mais frequentado é o {0}, o período que se encontra maior fluxo é o {1}.", elevador, periodo);
        }

        // Busca pelos andares menos requisitados
        private static void printAndarMenosUtilizado(List<UserInput> inputs)
        {
            List<int> result = new List<int>();
            // inicializa com o maximo possivel
            int minQtdeAcessos = inputs.Count();
            // para cada andar, conta quantas entradas possui na lista
            for (int andar = 0; andar <= QTDE_ANDARES; andar++)
            {
                int aux = inputs.Count(e => e.andar == andar);
                if (aux < minQtdeAcessos)
                {
                    result.Clear();
                    minQtdeAcessos = aux;
                }
                if (aux == minQtdeAcessos)
                {
                    result.Add(andar);
                }
            }
            Console.WriteLine("Com {0} requisições, o(s) andar(es) menos utilizado(s) pelos usuários é(são): [{1}]", minQtdeAcessos, String.Join(", ", result));
        }

        private static List<UserInput> readInput()
        {
            List<UserInput> inputs = new List<UserInput>();

            Console.WriteLine("Entrada de dados para sistema de levantamento.");
            Console.WriteLine("Digite os valores de entrada no padrão [{Andar}{Elevador:1}{Periodo:1},");
            Console.WriteLine("  Deixe o input em branco para terminar:");
            String input = Console.ReadLine();
            while (!String.IsNullOrEmpty(input))
            {
                try
                {
                    inputs.Add(processUserInput(input));
                    Console.WriteLine("Input válido, aguardando o próximo.");
                }
                catch (InvalidInputException ex)
                {
                    Console.WriteLine("Input apresenta o seguinte erro:");
                    Console.WriteLine(ex.Message);
                    Console.WriteLine("Tente Novamente");
                }
                finally
                {
                    input = Console.ReadLine();
                }
            }
            Console.WriteLine("Input Finalizado.");
            return inputs;
        }
        private static UserInput processUserInput(String input)
        {
            input = input.Trim();
            if (input.Length < 3)
                throw new InvalidInputException(String.Format("Input {0} é muito pequeno para ser válido.", input));

            //Teste de período
            UserInput result = new UserInput();
            String aux = input.Last().ToString();
            if ("M".Equals(aux))
                result.periodo = PeriodoEnum.MATUTINO;
            else if ("V".Equals(aux))
                result.periodo = PeriodoEnum.VESPERTINO;
            else if ("N".Equals(aux))
                result.periodo = PeriodoEnum.NOTURNO;
            else
                throw new InvalidInputException(String.Format("Periodo {0} não é válido. Periodos válidos [M,V,N].", aux));

            // Teste para Elevador
            input = input.Substring(0, input.Length - 1);
            aux = input.Last().ToString();
            if (ELEVADORES.Contains(aux))
                result.elevador = aux;
            else
                throw new InvalidInputException(String.Format("Elevador {0} não é válido. Elevadores válidos [{1}].", aux, String.Join(", ", ELEVADORES)));

            input = input.Substring(0, input.Length - 1);
            int value = 0;
            if (Int32.TryParse(input, out value))
            {
                if (value <= QTDE_ANDARES && value >= 0)
                    result.andar = value;
                else
                    throw new InvalidInputException(String.Format("Andar {0} não é válido. O prédio possui apenas {1} andares, iniciando em 0.", aux, QTDE_ANDARES));
            }
            else
                throw new InvalidInputException(String.Format("Andar {0} não é válido. Número inválido.", aux));

            return result;
        }
    }

    class UserInput
    {
        public int andar { get; set; }
        public String elevador { get; set; }
        public PeriodoEnum periodo { get; set; }
    }

    enum PeriodoEnum
    {
        MATUTINO, VESPERTINO, NOTURNO
    }

    //Exception gerada quando informado uma entrada inválida
    public class InvalidInputException : Exception
    {
        public InvalidInputException(string message) : base(message) { }
    }
}
